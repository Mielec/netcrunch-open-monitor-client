# NetCrunch Open Monitor Client

This program allows you to send counters to your NetCrunch Open Monitor instance.
<https://www.adremsoft.com/netcrunch/article/open-monitor>

## Building

### Building with tests:
Project contains some integration tests written with TestFX (<https://github.com/TestFX/TestFX>) project. It will display some windows during build.

```
  mvn clean install
```

### Building without tests:

```
mvn clean install -Dmaven.test.skip=true
```

## Running

Application build ```monitor-client-1.0-SNAPSHOT.jar``` file into ```/target``` directory, and require latest JVM 8 with JavaFX.  

```
java -jar monitor-client-1.0-SNAPSHOT.jar
```