package pl.mielecmichal.monitor.client.controllers;

import com.squareup.okhttp.OkHttpClient;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import org.apache.commons.validator.routines.IntegerValidator;
import pl.mielecmichal.monitor.client.MonitorClientApplication;
import pl.mielecmichal.monitor.client.models.NetCrunchConnection;
import pl.mielecmichal.monitor.client.models.NetCrunchConnector;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ConnectingController implements Initializable {

    @FXML
    private TextField hostField;

    @FXML
    private TextField portField;

    @FXML
    private TextField apiKeyField;

    @FXML
    private ComboBox schemeComboBox;

    @FXML
    private Separator separator;

    @FXML
    private VBox vBox;

    private Label invalidHostLabel = new Label("Host is invalid! (should be localhost | www.example.pl )");

    private Label invalidPortLabel = new Label("Port is invalid! (should be between 1 and 65535)");

    private Label connectionProblemLabel = new Label("Could not connect to monitor.");

    public static final String HTTP_PROTOCOL = "http";
    public static final String HTTPS_PROTOCOL = "https";

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList schemes = FXCollections.observableArrayList(HTTP_PROTOCOL, HTTPS_PROTOCOL);
        schemeComboBox.setItems(schemes);
        schemeComboBox.valueProperty().setValue(HTTP_PROTOCOL);
    }

    @FXML
    public void loginButtonClickHandler(ActionEvent even) throws IOException {
        String scheme = (String) schemeComboBox.getValue();
        String host = hostField.getText();
        String port = portField.getText();
        String apiKey = apiKeyField.getText();

        if (!isValidFields()) {
            return;
        }

        NetCrunchConnection connection = new NetCrunchConnection(scheme, host, port, apiKey);
        NetCrunchConnector connector = new NetCrunchConnector(connection, new OkHttpClient());

        if (connector.establishConnection()) {
            MonitorClientApplication.getInstance().showSendingView(connector);
            return;
        }
        insertErrorLabel(connectionProblemLabel);
    }

    private boolean isValidFields() {

        boolean isValid = true;
        removeLabel(invalidHostLabel);
        removeLabel(invalidPortLabel);

        String host = hostField.getText();
        if (host == null || host.isEmpty()) {
            insertErrorLabel(invalidHostLabel);
            isValid = false;
        }

        String port = portField.getText();
        if (!isValidPort(port)) {
            insertErrorLabel(invalidPortLabel);
            isValid = false;
        }

        return isValid;
    }

    private void insertErrorLabel(Label label) {
        label.setTextFill(Color.RED);
        int index = vBox.getChildren().indexOf(separator);
        vBox.getChildren().add(index + 1, label);
    }

    private void removeLabel(Label label) {
        vBox.getChildren().remove(label);
    }

    private boolean isValidPort(String port) {
        IntegerValidator integerValidator = new IntegerValidator();
        boolean isValidPort = integerValidator.isValid(port);
        if (!isValidPort) {
            return false;
        }

        return integerValidator.isInRange(Integer.parseInt(port), 1, 65535);
    }
}