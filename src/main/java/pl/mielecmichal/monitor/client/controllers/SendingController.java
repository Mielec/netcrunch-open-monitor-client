package pl.mielecmichal.monitor.client.controllers;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import pl.mielecmichal.monitor.client.models.NetCrunchConnector;
import pl.mielecmichal.monitor.client.models.NetCrunchCounter;

import java.io.IOException;
import java.net.URL;
import java.time.Duration;
import java.util.ResourceBundle;

public class SendingController implements Initializable {

    private NetCrunchConnector netCrunchConnector;

    @FXML
    private TextField counterNameField;

    @FXML
    private TextField counterValueField;

    @FXML
    private ListView countersListView;

    @FXML
    private ProgressBar sendingProgressBar;

    @FXML
    private Slider intervalSlider;

    @FXML
    private Label intervalLabel;

    @FXML
    private Button startSendingButton;

    @FXML
    private Button stopSendingButton;

    private ObservableList<NetCrunchCounter> netCrunchCounters = FXCollections.synchronizedObservableList(FXCollections.observableArrayList());

    public void setNetCrunchConnector(NetCrunchConnector netCrunchConnector) {
        this.netCrunchConnector = netCrunchConnector;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        countersListView.setItems(netCrunchCounters);
        intervalSlider.valueProperty().addListener((observable, oldValue, newValue) -> intervalLabel.setText(String.valueOf(newValue.intValue())));
        stopSendingButton.setDisable(true);
    }

    public void addButtonAction(ActionEvent event) {
        NetCrunchCounter counter = readCounterFromFields();
        if (counter == null) {
            return;
        }

        if (netCrunchCounters.stream().anyMatch(c -> c.equals(counter))) {
            return;
        }

        netCrunchCounters.add(counter);
    }

    public void editButtonAction(ActionEvent event) {
        Object selectedItem = countersListView.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            return;
        }

        NetCrunchCounter counter = readCounterFromFields();
        if (counter == null) {
            return;
        }

        int index = netCrunchCounters.indexOf(selectedItem);
        netCrunchCounters.set(index, counter);
    }

    public void deleteButtonAction(ActionEvent event) {
        Object selectedItem = countersListView.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            return;
        }
        netCrunchCounters.remove(selectedItem);
    }

    public void countersListViewClicked(MouseEvent event) {
        Object selectedItem = countersListView.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            return;
        }

        NetCrunchCounter counter = (NetCrunchCounter) selectedItem;
        counterNameField.setText(counter.getName());
        counterValueField.setText(counter.getValue());


    }

    private NetCrunchCounter readCounterFromFields() {
        String name = counterNameField.getText();
        if (name.isEmpty()) {
            return null;
        }

        String value = counterValueField.getText();
        if (value.isEmpty()) {
            return null;
        }

        return new NetCrunchCounter(name, value);

    }

    public void startSendingButtonAction(ActionEvent event) {
        startSendingButton.setDisable(true);
        stopSendingButton.setDisable(false);
        intervalSlider.setDisable(true);

        Callback afterSendCallback = new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                showSendingError();
            }

            @Override
            public void onResponse(Response response) throws IOException {
                showSendingSuccess();
            }
        };

        Duration duration = Duration.ofSeconds(intervalSlider.valueProperty().intValue());

        netCrunchConnector.startSending(netCrunchCounters, duration, afterSendCallback);
    }


    private void showSendingError() {
        sendingProgressBar.setStyle("-fx-accent: red");
        animateProgressBar();
    }

    private void showSendingSuccess() {
        sendingProgressBar.setStyle("-fx-accent: green");
        animateProgressBar();
    }

    private void animateProgressBar() {
        sendingProgressBar.setProgress(0.0);
        Timeline timeline = new Timeline();
        KeyValue keyValue = new KeyValue(sendingProgressBar.progressProperty(), 1.0);
        KeyFrame keyFrame = new KeyFrame(new javafx.util.Duration(300), keyValue);
        timeline.getKeyFrames().add(keyFrame);
        timeline.play();
        timeline.setOnFinished((event) -> sendingProgressBar.setProgress(0.0));
    }

    public void stopSendingButtonAction(ActionEvent event) {
        stopSendingButton.setDisable(true);
        startSendingButton.setDisable(false);
        intervalSlider.setDisable(false);
        netCrunchConnector.stopSending();
    }

}
