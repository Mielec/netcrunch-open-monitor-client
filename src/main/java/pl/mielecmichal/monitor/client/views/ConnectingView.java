package pl.mielecmichal.monitor.client.views;

import javafx.stage.Stage;

public class ConnectingView extends BaseView {

    public ConnectingView(Stage primaryStage) {
        super(primaryStage);
    }

    @Override
    public String fxmlName() {
        return "/connecting-view.fxml";
    }
}
