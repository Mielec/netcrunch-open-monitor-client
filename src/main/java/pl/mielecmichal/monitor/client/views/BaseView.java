package pl.mielecmichal.monitor.client.views;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public abstract class BaseView {

    public static final double HEIGHT = 800.0;
    public static final double WIDTH = 600.0;

    protected Stage primaryStage;

    public BaseView(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public Object load() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(fxmlName()));
        Parent parent = loader.load();
        Scene scene = new Scene(parent);
        primaryStage.setScene(scene);
        primaryStage.setWidth(WIDTH);
        primaryStage.setHeight(HEIGHT);
        return loader.getController();
    }

    public void show() {
        primaryStage.show();
    }

    public abstract String fxmlName();
}
