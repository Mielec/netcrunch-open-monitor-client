package pl.mielecmichal.monitor.client.views;

import javafx.stage.Stage;
import pl.mielecmichal.monitor.client.controllers.SendingController;
import pl.mielecmichal.monitor.client.models.NetCrunchConnector;

import java.io.IOException;

public class SendingView extends BaseView {

    private final NetCrunchConnector connector;

    public SendingView(Stage primaryStage, NetCrunchConnector connector) {
        super(primaryStage);
        this.connector = connector;
    }

    @Override
    public Object load() throws IOException {
        SendingController controller = (SendingController) super.load();
        controller.setNetCrunchConnector(connector);
        return controller;
    }

    @Override
    public String fxmlName() {
        return "/sending_view.fxml";
    }
}
