package pl.mielecmichal.monitor.client.models;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.*;

import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class NetCrunchConnector extends TimerTask {

    private NetCrunchConnection connection;

    private List<NetCrunchCounter> counters;

    private Callback afterSendCallback;

    private Boolean isConnectionEstablished;

    private ScheduledExecutorService sendingExecutor;

    private ObjectMapper objectMapper = new ObjectMapper();

    private OkHttpClient okHttpClient;

    private final HttpUrl.Builder commonUrlBuilder;


    public NetCrunchConnector(NetCrunchConnection connection, OkHttpClient okHttpClient) {

        this.connection = connection;

        commonUrlBuilder = new HttpUrl.Builder()
                .scheme(connection.getScheme())
                .host(connection.getHost())
                .port(Integer.parseInt(connection.getPort()))
                .addEncodedPathSegment("ncintf")
                .addEncodedPathSegment("rest")
                .addEncodedPathSegment("1")
                .addEncodedPathSegment("openmon")
                .addQueryParameter("apikey", connection.getApiKey());

        this.okHttpClient = okHttpClient;
    }

    public boolean establishConnection() {

        if (isConnectionEstablished != null) {
            return isConnectionEstablished;
        }

        HttpUrl url = commonUrlBuilder
                .addEncodedPathSegment("counter")
                .build();

        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();

        Response response;
        try {
            response = okHttpClient.newCall(request).execute();
        } catch (IOException e) {
            return false;
        }

        if (response.isSuccessful()) {
            isConnectionEstablished = true;
            return true;
        }
        return false;
    }

    public void startSending(List<NetCrunchCounter> counters, Duration interval, Callback afterSendCallback) {
        if (!isConnectionEstablished) {
            throw new IllegalStateException("Connection is not established!");
        }

        this.afterSendCallback = afterSendCallback;
        this.counters = counters;
        this.sendingExecutor = Executors.newScheduledThreadPool(2);
        this.sendingExecutor.scheduleAtFixedRate(this, 1, interval.getSeconds(), TimeUnit.SECONDS);
    }

    public void stopSending() {
        if (!isConnectionEstablished) {
            throw new IllegalStateException("Connection is not established!");
        }

        sendingExecutor.shutdown();
    }

    private void sendCounters() {

        HttpUrl url = commonUrlBuilder
                .addEncodedPathSegment("update")
                .build();

        CountersDTO dto = new CountersDTO();
        dto.setApikey(connection.getApiKey());
        Map countersMap = counters.stream().collect(Collectors.toMap(NetCrunchCounter::getName, NetCrunchCounter::getValue));
        dto.setCounters(countersMap);

        String body;
        try {
            body = objectMapper.writeValueAsString(dto);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return;
        }

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), body);

        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();

        okHttpClient.newCall(request).enqueue(afterSendCallback);
    }

    @Override
    public void run() {
        sendCounters();
    }
}
