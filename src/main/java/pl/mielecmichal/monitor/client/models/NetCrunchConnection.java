package pl.mielecmichal.monitor.client.models;

public class NetCrunchConnection {

    private final String scheme;
    private final String host;
    private final String port;
    private final String apiKey;

    public NetCrunchConnection(String scheme, String host, String port, String apiKey) {
        this.scheme = scheme;
        this.host = host;
        this.port = port;
        this.apiKey = apiKey;
    }

    public String getScheme() {
        return scheme;
    }

    public String getHost() {
        return host;
    }

    public String getPort() {
        return port;
    }

    public String getApiKey() {
        return apiKey;
    }
}
