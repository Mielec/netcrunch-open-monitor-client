package pl.mielecmichal.monitor.client.models;

import java.util.Map;

public class CountersDTO {

    private String retain = "1";
    private String apikey;
    private Map<String, String> counters;

    public String getRetain() {
        return retain;
    }

    public void setRetain(String retain) {
        this.retain = retain;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public Map<String, String> getCounters() {
        return counters;
    }

    public void setCounters(Map<String, String> counters) {
        this.counters = counters;
    }
}
