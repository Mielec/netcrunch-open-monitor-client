package pl.mielecmichal.monitor.client.models;

public class NetCrunchCounter {

    private final String name;
    private final String value;

    public NetCrunchCounter(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return name + ": " + value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NetCrunchCounter counter = (NetCrunchCounter) o;

        if (name != null ? !name.equals(counter.name) : counter.name != null) return false;
        return value != null ? value.equals(counter.value) : counter.value == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
