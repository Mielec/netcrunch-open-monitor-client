package pl.mielecmichal.monitor.client;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import pl.mielecmichal.monitor.client.models.NetCrunchConnector;
import pl.mielecmichal.monitor.client.views.BaseView;
import pl.mielecmichal.monitor.client.views.ConnectingView;
import pl.mielecmichal.monitor.client.views.SendingView;

import java.io.IOException;

public class MonitorClientApplication extends Application {



    private static MonitorClientApplication instance;

    public MonitorClientApplication() {
        instance = this;
    }

    public static MonitorClientApplication getInstance() {
        return instance;
    }

    private Stage primaryStage;

    public void start(Stage primaryStage) throws IOException {
        this.primaryStage = primaryStage;
        showConnectingView();

        primaryStage.setOnCloseRequest(e -> {
            Platform.exit();
            System.exit(0);
        });
    }

    public void showConnectingView() throws IOException {
        BaseView view = new ConnectingView(primaryStage);
        showView(view);
    }

    public void showSendingView(NetCrunchConnector netCrunchConnector) throws IOException {
        BaseView view = new SendingView(primaryStage, netCrunchConnector);
        showView(view);
    }

    private void showView(BaseView view) throws IOException {
        view.load();
        view.show();
    }

}
