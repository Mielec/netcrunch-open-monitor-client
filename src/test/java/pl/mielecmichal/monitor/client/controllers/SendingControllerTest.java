package pl.mielecmichal.monitor.client.controllers;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import org.junit.Test;
import org.testfx.api.FxAssert;
import org.testfx.framework.junit.ApplicationTest;

import static org.testfx.api.FxAssert.verifyThat;

public class SendingControllerTest extends ApplicationTest {

    @Override
    public void start(Stage stage) throws Exception {
        Parent parent = FXMLLoader.load(getClass().getResource("/sending_view.fxml"));
        Scene scene = new Scene(parent);
        stage.setScene(scene);
        stage.show();
    }

    @Test
    public void shouldNotAddEmptyCounter() {
        //when
        rightClickOn("Add");

        //then
        verifyThat("#countersListView", (ListView l) -> l.getItems().isEmpty());
    }

    @Test
    public void shouldNotAddSameCounters() {
        //when
        clickOn("#counterNameField").write("NAME");
        clickOn("#counterValueField").write("VALUE");

        //then
        clickOn("Add");
        verifyThat("#countersListView", (ListView l) -> l.getItems().size() == 1);
        clickOn("Add");
        verifyThat("#countersListView", (ListView l) -> l.getItems().size() == 1);
    }

}