package pl.mielecmichal.monitor.client.models;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NetCrunchConnectorTest {

    private OkHttpClient okHttpClientMock;

    public static final NetCrunchConnection SOME_CONNECTION = new NetCrunchConnection("http", "adress", "80", "apikey");

    @Before
    public void setUp() {
        okHttpClientMock = mock(OkHttpClient.class);
    }

    @Test
    public void establishConnection_connectionProblem_shouldReturnFalse() throws IOException {
        // given
        Call callMock = mock(Call.class);
        when(callMock.execute()).thenThrow(new IOException());
        when(okHttpClientMock.newCall(any(Request.class))).thenReturn(callMock);

        NetCrunchConnector netCrunchConnector = new NetCrunchConnector(SOME_CONNECTION, okHttpClientMock);

        //when
        boolean result = netCrunchConnector.establishConnection();

        //then
        assertFalse(result);
    }
}